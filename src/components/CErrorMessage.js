import React from "react";
import styled from "styled-components";
const StyledDiv = styled.div`
    text-align: left;
    color: red;
    height: 10px;
`;
function CErrorMessage(props) {
    return <StyledDiv>{props.name && <span>This field is required</span>}</StyledDiv>;
}

export default CErrorMessage;
