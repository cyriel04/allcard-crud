import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import { Input } from "antd";
import { useForm, Controller } from "react-hook-form";
import CButton from "../components/CButton";
import logo from "../logo.svg";
import art from "../assets/images/arthur.jpg";

import axios from "axios";
import Auth from "../utils/Auth";
import CInput from "../components/CInput";
import CErrorMessage from "../components/CErrorMessage";

const StyleDiv = styled.div`
    background: white;
    display: flex;
    justify-content: flex-end;
    /* border: 1px red solid; */
    height: 100vh;
    .card {
        position: absolute;
        background: transparent;

        width: 500px;
        height: 100%;
        display: flex;
        justify-content: center;
        align-items: center;

        @media screen and (max-width: 500px) {
            width: 100%;
            background: red;
        }
        .panel {
            display: flex;
            justify-content: center;
            align-items: center;

            width: 400px;
            height: 600px;
            background: white;
            padding: 50px;

            .button-container {
                display: flex;
                flex-direction: column;
                justify-content: center;
                align-items: center;

                .ghost {
                    margin: 30px 0px;
                    width: 200px;
                }
            }
        }
    }
    @media (prefers-reduced-motion: no-preference) {
        .logo {
            animation: App-logo-spin infinite 20s linear;
        }
    }
    .bg {
        background-size: cover;
        width: 100%;
        height: 100vh;
        position: absolute;
    }
`;
function Login(props) {
    const { handleSubmit, control, errors } = useForm();
    let url = "/users";
    const onSubmit = async data => {
        console.log(data);
        const newData = await axios.post(url, data);
        Auth.login(() => {
            props.history.push("/home");
        });
        console.log("hello");
    };
    return (
        <StyleDiv>
            <img src={art} className="bg" alt="art" />

            <div className="card">
                <div className="panel">
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <img src={logo} className="logo" alt="logo" />
                        <CInput name="username" control={control} rules={{ required: true }} placeholder="Username" />
                        <CErrorMessage name={errors.username} />
                        <CInput name="password" control={control} rules={{ required: true }} placeholder="Password" />
                        <CErrorMessage name={errors.password} />

                        <div className="button-container">
                            <CButton htmlType="submit" title="LOGIN"></CButton>
                            <p>
                                Click here to <Link to="/register">Register</Link>
                            </p>
                        </div>
                    </form>
                </div>
            </div>
        </StyleDiv>
    );
}

export default Login;
