import React from "react";
import { Button } from "antd";
import styled from "styled-components";
const StyledDiv = styled.div`
    .ant-btn-primary {
        color: #000000;
        margin: 20px 0px;
        width: 200px;
        height: 40px;
        border-radius: 10px;
        border-color: #eeeeee;
        background: white;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        &:focus {
            color: #000000;
            border-color: #eeeeee;
            background: transparent;
        }
        &:hover {
            color: #eeeeee;
            background: #f7931e;
            border-color: #f7931e;
        }
    }
`;
function CButton(props) {
    return (
        <StyledDiv>
            <Button htmlType={props.htmlType} onClick={props.onClick} type="primary">
                {props.title}
            </Button>
        </StyledDiv>
    );
}

export default CButton;
