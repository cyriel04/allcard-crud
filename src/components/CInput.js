import React from "react";
import { Input } from "antd";
import { Controller } from "react-hook-form";
import styled from "styled-components";
const StyledDiv = styled.div`
    .ant-input {
        width: 300px;
        margin: 10px 0px 4px 0px;
        height: 50px;
        border-color: #000000;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }
`;
export default function CInput(props) {
    return (
        <StyledDiv>
            <Controller as={<Input name={props.name} />} name={props.name} control={props.control} rules={props.rules} placeholder={props.placeholder} defaultValue={props.defaultValue} />
        </StyledDiv>
    );
}
