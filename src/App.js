import React, { Fragment } from "react";
import { Route, Switch, BrowserRouter as Router } from "react-router-dom";
import { Layout } from "antd";
import "antd/dist/antd.css";
import Home from "../src/pages/Home";
import Login from "../src/pages/Login";
import Register from "../src/pages/Register";
import NotFound from "../src/pages/NotFound";
import Navigation from "../src/components/Navigation";
import "./App.css";
import PrivateRoute from "./PrivateRoute";
const { Content } = Layout;
function App() {
    return (
        <div className="App">
            <Router>
                <Switch>
                    <Route name="login" exact path="/" component={Login}></Route>
                    <Route name="register" path="/register" component={Register}></Route>
                    <Fragment>
                        <Layout className="layout">
                            <Navigation />
                            <Content style={{ padding: "0 50px" }}>
                                <Switch>
                                    {/* <Route name="home" path="/home" component={Home}></Route> */}
                                    <PrivateRoute name="home" path="/home" component={Home} />
                                    <PrivateRoute component={NotFound}></PrivateRoute>
                                </Switch>
                            </Content>
                        </Layout>
                    </Fragment>
                </Switch>
            </Router>
        </div>
    );
}

export default App;
