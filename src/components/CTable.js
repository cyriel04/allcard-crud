import React from "react";
import { Table } from "antd";
import styled from "styled-components";

const StyledDiv = styled.div`
    .ant-table-wrapper {
        margin: 0 auto;
        width: 600px;
        overflow: auto;
        height: 460px;
        border-color: red;
        .ant-table-thead {
            th {
                background: #4e4b48;
                color: white;
            }
        }
        .ant-table-body {
            border: 1px solid #4e4b48;

            tr:nth-child(odd) {
                td {
                    background: #dddddd;
                    color: #4e4b48;
                }
            }
        }
    }
`;

export default function CTable(props) {
    return (
        <StyledDiv>
            <Table dataSource={props.dataSource} pagination={props.pagination}>
                {props.children}
            </Table>
        </StyledDiv>
    );
}
