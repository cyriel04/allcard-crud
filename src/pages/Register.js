import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import { Input } from "antd";
import { useForm, Controller } from "react-hook-form";
import CButton from "../components/CButton";
import axios from "axios";
import logo from "../logo.svg";

const StyleDiv = styled.div`
    background: white;
    height: 90vh;
    display: flex;
    align-items: center;
    .card {
        background: gray;
        width: 600px;
        margin: 0 auto;
        border: 1px solid black;

        @media screen and (max-width: 500px) {
            width: 100%;
            background: red;
        }
        .panel {
            margin: 0 auto;

            display: flex;
            justify-content: center;
            align-items: center;

            width: 400px;
            height: inherit;
            background: white;
            padding: 50px;

            .ant-input {
                margin: 10px 0px;
                height: 50px;
                border-color: #000000;
                box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            }
            .button-container {
                display: flex;
                flex-direction: column;
                justify-content: center;
                align-items: center;

                .ghost {
                    margin: 20px 0px;
                    width: 200px;
                }
            }
        }
    }
`;
function Register(props) {
    const { handleSubmit, control, errors } = useForm();
    let url = "/users";
    const onSubmit = async data => {
        try {
            const newData = await axios.post(url, data);
            props.history.push("/home");
        } catch (error) {
            console.log(error);
        }
    };
    return (
        <StyleDiv>
            <div className="card">
                <div className="panel">
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <img src={logo} className="logo" alt="logo" />
                        <Controller as={<Input name="username" />} name="username" control={control} rules={{ required: true }} placeholder="Username" />
                        {errors.username && <span>This field is required</span>}
                        <Controller as={<Input name="password" />} name="password" control={control} rules={{ required: true }} placeholder="Password" />
                        {errors.password && <span>This field is required</span>}

                        <div className="button-container">
                            <CButton htmlType="submit" title="REGISTER"></CButton>
                            <Link to="/">
                                <CButton title="BACK TO LOGIN"></CButton>
                            </Link>
                        </div>
                    </form>
                </div>
            </div>
        </StyleDiv>
    );
}

export default Register;
