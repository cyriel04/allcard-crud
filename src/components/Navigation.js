import React from "react";
import { Menu } from "antd";
import { Link } from "react-router-dom";
import styled from "styled-components";

const { SubMenu } = Menu;
const StyledDiv = styled.div`
    .ant-menu {
        background: #d9d9d9;
        .right {
            float: right;
        }
    }
`;
function Navigation() {
    return (
        <StyledDiv>
            <Menu mode="horizontal">
                {/* <Menu.Item key="mail">Navigation One</Menu.Item>
                <SubMenu title={<span className="submenu-title-wrapper">Navigation Three - Submenu</span>}>
                    <Menu.ItemGroup title="Item 1">
                        <Menu.Item key="setting:1">Option 1</Menu.Item>
                        <Menu.Item key="setting:2">Option 2</Menu.Item>
                    </Menu.ItemGroup>
                </SubMenu> */}

                <Menu.Item key="alipay" className="right">
                    <Link to="/">Logout</Link>
                </Menu.Item>
            </Menu>
        </StyledDiv>
    );
}

export default Navigation;
