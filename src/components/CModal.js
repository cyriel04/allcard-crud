import React from "react";
import { Modal } from "antd";
import styled from "styled-components";
import { useForm } from "react-hook-form";
import CButton from "./CButton";
import CInput from "./CInput";

const StyledModal = styled(Modal)`
    .ant-modal-body {
        height: 400px;
        margin-top: 100px;
    }
`;
function CModal(props) {
    const { handleSubmit, control, errors } = useForm();

    return (
        <StyledModal key={props.singleData.key} visible={props.visible} footer={null} onCancel={props.onCancel}>
            <form onSubmit={handleSubmit(props.handleSubmit)}>
                <h1>{props.title}</h1>
                <CInput name="title" control={control} rules={{ required: true }} placeholder="Title" defaultValue={props.singleData.title} />
                {errors.username && <span>This field is required</span>}
                <CInput name="description" control={control} rules={{ required: true }} placeholder="Description" defaultValue={props.singleData.description} />
                {errors.password && <span>This field is required</span>}
                <div className="button-container">
                    <CButton htmlType="submit" title="Submit"></CButton>
                </div>
            </form>
        </StyledModal>
    );
}

export default CModal;
