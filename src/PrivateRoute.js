import React from "react";
import Auth from "./utils/Auth";
import { Redirect, Route } from "react-router-dom";

const PrivateRoute = ({ component: Component, ...rest }) => {
    // Add your own authentication on the below line.
    const isLoggedIn = Auth.isAuth();

    return <Route {...rest} render={props => (isLoggedIn ? <Component {...props} /> : <Redirect to={{ pathname: "/", state: { from: props.location } }} />)} />;
};

export default PrivateRoute;
