import React, { useState, useEffect } from "react";
import { Table } from "antd";
import axios from "axios";
import styled from "styled-components";
import CButton from "../components/CButton";
import CTable from "../components/CTable";
import CModal from "../components/CModal";

const { Column } = Table;

const StyledDiv = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    height: 90vh;

    .container {
        width: 600px;
        display: flex;
        justify-content: center;
        flex-direction: column;
        align-items: center;
        .btn-container {
            padding-top: 100px;
            display: flex;
            width: 100%;
        }
        .action-container {
            display: flex;
            .ant-btn-primary {
                width: 100px;
                margin-right: 10px;
            }
        }
    }
`;

function Home() {
    const [data, setData] = useState([]);
    const [singleData, setSingleData] = useState({});
    const [edit, setEdit] = useState(false);

    let url = "/posts";

    useEffect(() => {
        async function fetchMyAPI() {
            const response = await axios.get(url);
            const wow = response.data.map(a => ({
                key: a.id,
                id: a.id,
                title: a.title,
                description: a.description,
                date: a.date
            }));
            setData(wow);
        }

        fetchMyAPI();
    }, [url]);

    const handleAdd = async () => {
        const value = {
            title: "GG",
            description: "gggggg",
            date: "wwww"
        };
        const newData = await axios.post(url, value);
        value.key = newData.data.id;
        value.id = newData.data.id;

        setData([...data, value]);
    };

    const handleDelete = async (e, value) => {
        const x = await axios.delete(`${url}/${value.id}`);
        const newData = [...data.filter(data => data.id !== value.id)];
        setData(newData);
    };

    const handleEdit = (e, value) => {
        setEdit(true);
        setSingleData(value);
    };
    const handleSubmitEdit = async value => {
        const newData = {
            id: singleData.id,
            key: singleData.id,
            title: value.title,
            description: value.description
        };
        const update = data.map(item => {
            if (item.id === singleData.id) {
                return newData;
            }
            return item;
        });
        const x = await axios.put(`${url}/${singleData.id}`, newData);
        await setData(update);
        await setEdit(false);
        return update;
    };

    return (
        <StyledDiv>
            <div className="container">
                <div className="btn-container">
                    <CButton title="Add" type="orange-btn" onClick={handleAdd}></CButton>
                </div>
                <CTable dataSource={data} pagination={false}>
                    <Column title="Title" dataIndex="title" key="title" />
                    <Column title="Description" dataIndex="description" key="description" />
                    <Column title="Date" dataIndex="date" key="date" />
                    <Column
                        key="actions"
                        title="Actions"
                        width={140}
                        render={data => (
                            <div className="action-container">
                                <CButton title="Edit" type="ghost-btn" onClick={e => handleEdit(e, data)}>
                                    Edit
                                </CButton>
                                <CButton title="Delete" type="red-btn" onClick={e => handleDelete(e, data)}>
                                    Delete
                                </CButton>
                            </div>
                        )}
                    />
                </CTable>
            </div>
            <CModal title="Edit" visible={edit} singleData={singleData} onCancel={() => setEdit(false)} handleSubmit={handleSubmitEdit} />
        </StyledDiv>
    );
}

export default Home;
